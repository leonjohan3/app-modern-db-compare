#!/bin/false

if ! find $ORACLE_HOME -name sqlplus -type f -executable > $TEMP_FILE 2> /dev/null ; then
    :
fi

SQLPLUS=$(<$TEMP_FILE)

if [ "$(uname -p)" == "sparc" ] ; then
    TNS_OF_INTEREST=${TNS_OF_INTEREST:-TNS_USED_BY_THE_SPARC_SERVER}
else
    TNS_OF_INTEREST=${TNS_OF_INTEREST:-XE}
fi

if [ "$TNS_OF_INTEREST" == "XE" ] ; then
    readonly USER_PASS=scott/tiger
else
    readonly USER_PASS=scott_on_the_sparc_server/tiger_on_the_sparc_server
fi

readonly SQLPLUS_COMMAND="$SQLPLUS -s ${USER_PASS}@$TNS_OF_INTEREST"
