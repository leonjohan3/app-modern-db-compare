#!/bin/false

TEMP_FILE=$(mktemp)

for I in {1..1000} ; do
    if [ -f $TEMP_FILE ] ; then
        break
    fi
done

test -f $TEMP_FILE || exit 1
