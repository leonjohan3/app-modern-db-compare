#!/bin/bash

# Treat unset variables as an error when performing parameter expansion
# and exit immediately if a simple command exits with a non-zero status.
# Returns the exit value of the last (rightmost) command to exit with 
# non-zero in a pipeline of commands.
set -ue

# Some notes on bash scripting:
# Variables flagged as local readonly will not terminate the script if they are set by command substitution.

trap 'rm -f $TEMP_FILE' EXIT

readonly RESULT_DIR=results-table-layouts
mkdir $RESULT_DIR

TEMP_FILE=unassigned
source util/assign-and-create-temp-file.sh
source util/set-env-vars.sh

for USR_FILE in results-table-lists/*.txt ; do
    
    USR=$(basename $USR_FILE .txt)
    mkdir $RESULT_DIR/$USR

    while read TABLE ; do
        echo "set termout off" > $TEMP_FILE
        echo "spool $RESULT_DIR/$USR/${TABLE}.txt" >> $TEMP_FILE
        echo "desc ${USR}.${TABLE};" >> $TEMP_FILE
        echo "exit;" >> $TEMP_FILE
        $SQLPLUS_COMMAND @${TEMP_FILE}
    done < $USR_FILE
done
