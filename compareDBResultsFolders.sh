#!/bin/bash

# Treat unset variables as an error when performing parameter expansion
# and exit immediately if a simple command exits with a non-zero status.
# Returns the exit value of the last (rightmost) command to exit with 
# non-zero in a pipeline of commands.
set -ue

# Some notes on bash scripting:
# Variables flagged as local readonly will not terminate the script if they are set by command substitution.

if [ $# -lt 2 ] ; then
    echo "Usage: $0 folderA folderB"
    echo "folderA is the folder where the results-* folders of the 1st database to compare is found"
    echo "folderB is the folder where the results-* folders of the 2nd database to compare is found"
    exit 1
fi

FOLDER_A=$1
FOLDER_B=$2
EXIT_CODE=0

cd $FOLDER_A

for DI in results-* ; do
    if [ -d $DI ] ; then
        pushd $DI > /dev/null
        
        for FI in $(find . -type f) ; do
            if ! diff -q $FI $FOLDER_B/$DI/$FI ; then
                EXIT_CODE=1
            fi
        done

        popd > /dev/null
    fi
done

exit $EXIT_CODE
