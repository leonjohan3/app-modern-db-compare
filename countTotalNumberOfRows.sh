#!/bin/bash

# Treat unset variables as an error when performing parameter expansion
# and exit immediately if a simple command exits with a non-zero status.
# Returns the exit value of the last (rightmost) command to exit with 
# non-zero in a pipeline of commands.
set -ue

# Some notes on bash scripting:
# Variables flagged as local readonly will not terminate the script if they are set by command substitution.

cd results-row-counts-per-table
TOTAL_ROW_CNT=0

for USR_FILE in *.txt ; do
    for ROW_CNT in $(awk -F '~' '{print $2}' $USR_FILE) ; do
        TOTAL_ROW_CNT=$(( $TOTAL_ROW_CNT + $ROW_CNT ))
    done
done

echo "Total number of rows: $TOTAL_ROW_CNT"
