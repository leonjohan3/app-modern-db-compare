#!/bin/bash

# Treat unset variables as an error when performing parameter expansion
# and exit immediately if a simple command exits with a non-zero status.
# Returns the exit value of the last (rightmost) command to exit with 
# non-zero in a pipeline of commands.
set -ue

# Some notes on bash scripting:
# Variables flagged as local readonly will not terminate the script if they are set by command substitution.

trap 'rm -f $TEMP_FILE' EXIT

TEMP_FILE=unassigned
source util/assign-and-create-temp-file.sh

cd results-row-counts-per-table
> $TEMP_FILE

for USR_FILE in *.txt ; do
    USR=$(basename $USR_FILE .txt)
    sed -e "s,^.*$,$USR.&," -e 's,~, ,' $USR_FILE >> $TEMP_FILE
done

sort -nrk 2 $TEMP_FILE | head -10
