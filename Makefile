.PHONY: run
run:	results-row-counts-per-table results-table-layouts results-ora-object-names results-index-sizes results-invalid-objects \
        results-source-number-of-rows results-view-definitions
	@echo "Once information gathering has been done for both DBs, run ./compareDBResultsFolders.sh folderForDBa folderForDBb"

results-view-definitions:
	./getViewDefinitions.sh

results-source-number-of-rows:
	./getSourceNumberOfRows.sh

results-invalid-objects:
	./getInvalidObjects.sh

results-index-sizes:
	./getIndexSizes.sh

results-ora-object-names:
	./getOracleObjectNames.sh

results-table-layouts:	results-table-lists
	./getTableLayouts.sh

results-row-counts-per-table:	results-table-lists
	./countNumberOfRowsPerTable.sh

results-table-lists:
	./getTablesForUsers.sh

.PHONY: clean
clean:
	rm -rf results-*
