## What is this?
As part of application modernisation, one might need to also perform a database conversion (i.e. upgrading from Oracle version 8 to 10). The
Oracle sqlplus scripts are used to extract key values from both a source and destination database, and to compare these values to ensure that
all data and other objects were converted successfully from the old to the new database. The following Oracle objects are checked: tables, views,
sequences, indexes, procedures and triggers.

## How to use?
- Make sure the Oracle connection setting are correct in the util/set-env-vars.sh. Run sqlplus manually with these params to confirm.
- Run "make clean" to initialize the folder
- Run "make" to perform the information gathering.
- On Solaris GNU make might be here: /.SUNWnative/usr/sfw/bin/gmake
- Once run for both old and new environments compare the files in results-* using the compareDBResultsFolders.sh script.

## Prerequisites
- sqlplus (see location and other expected settings in the util/set-env-vars.sh file)
- a valid tnsnames.ora file
- GNU make and find

## Version control
- run "git config -l"
