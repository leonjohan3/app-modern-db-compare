#!/bin/bash

# Treat unset variables as an error when performing parameter expansion
# and exit immediately if a simple command exits with a non-zero status.
# Returns the exit value of the last (rightmost) command to exit with 
# non-zero in a pipeline of commands.
set -ue

# Some notes on bash scripting:
# Variables flagged as local readonly will not terminate the script if they are set by command substitution.

trap 'rm -f $TEMP_FILE' EXIT

readonly RESULT_DIR=results-view-definitions
mkdir $RESULT_DIR

TEMP_FILE=unassigned
source util/assign-and-create-temp-file.sh
source util/set-env-vars.sh

for USR in $(grep -v ^# userlist.txt) ; do    
    cat util/sqlplus-prefix.txt > $TEMP_FILE
    echo "spool $RESULT_DIR/$USR.txt" >> $TEMP_FILE
    echo "SELECT view_name||'~'||text_length FROM all_views WHERE owner='$USR' ORDER BY view_name;" >> $TEMP_FILE
    echo "exit;" >> $TEMP_FILE
    $SQLPLUS_COMMAND @${TEMP_FILE}
done
